package com.oreillyauto.domain;

import java.io.Serializable;

public class Widget implements Serializable{

    private static final long serialVersionUID = 7221088945385366885L;

    private String firstName;
    private String lastName;
    private int age;
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    @Override
    public String toString() {
        return "Widget [firstName=" + firstName + ", lastName=" + lastName + ", age=" + age + "]";
    }
    
}
