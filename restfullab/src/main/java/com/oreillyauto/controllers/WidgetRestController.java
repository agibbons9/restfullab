package com.oreillyauto.controllers;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.oreillyauto.domain.Widget;

@RestController
public class WidgetRestController {
    
    @PostMapping(value = "/widget", produces = MediaType.APPLICATION_JSON_VALUE)
    public Widget getOneWidget() {
        Widget widget = new Widget();
        widget.setFirstName("Widget");
        return widget;
    }
    
}
